// instanciation 
var game = new SweatyPromoClient.offline()

const bounce = new Audio('./src/assets/sound/bounce.mp3')


// les src des images
var player = './src/assets/img/billyboy.gif'
var goutte = './src/assets/img/zippyboy.gif'
var empty = './src/assets/img/empty.png'
var dead = './src/assets/img/facepalm.png'



// la matrice qui tourne en boucle
game.on('matrix', (matrix) => {
    // double loop pour avoir les 2 index de la matrice [][]

    for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < matrix[i].length; j++) {
            // if (!isPlayerInGame) {
            // }
            if (matrix[i][j] == 1) {
                $(`#cell${i}${j}`).attr('src', goutte)
                $(`#cell${i}${j}`).addClass('rotatage')

            } else if (matrix[i][j] == 0) {
                $(`#cell${i}${j}`).attr('src', empty)
                $(`#cell${i}${j}`).removeClass('rotatage')
            }
            if (i == 7 && j == game.getPosition()) {
                $(`#cell${i}${j}`).attr('src', player)
            } else if (i == 7 && j == game.getPosition()) {
                $(`#cell${i}${j}`).attr('src', dead)
            }
        }
    } if (!patapon) {
        iaDemo(matrix)
    }
})

// methode de l'objet attaché aux boutons
$('#start').on('click', function () {
    game.start()
    
    $('#start').addClass('hidden')
    console.log('start')
    $('#login').addClass('hidden')
})

$('#left').on('click', function () {
    game.left()
    bounce.play()
    bounce.volume = 0.4
})


$('#right').on('click', function () {
    game.right()
    bounce.play()
    bounce.volume = 0.4
})
// bouton envoi de msg
$('#send').on('click', function(){
    let msg = $('#msg').val()
    game.sendMessage(msg)
    console.log(`send ${msg}`)
    $('#msg').val(null)
})
// droite et gauche avec les fleche du clavier
$(document).keyup(function(e){
    let vey = e.which
    if(vey == 37){
        game.left()
    } else if (vey == 39) {
        game.right()
    }
})

// bouton pour log in
$('#login').on('click', function () {
    game.login()
    $('#login').addClass('hidden')
})

// methode de l'objet qui annonce la fin du game
game.on('sweaty', function () {
    console.log('perdu')
    $(`#cell7${game.getPosition()}`).attr('src', dead)
    $('#start').removeClass('hidden')
})
// recup les msg du serv ?
game.on('message', function(messageData){
    let name = messageData.author;
    let msg = messageData.message
    $('#logs').append(`<p>${name}: ${msg}</p>`)
})

var gameMode = $('#patapon')
let patapon = false
// fonction qui toggle l'ia
gameMode.on('click', function () {
    if (patapon == false) {
        patapon = true
        gameMode.text('oeuf')
    } else {
        patapon = false
        gameMode.text('farine')
    }
})